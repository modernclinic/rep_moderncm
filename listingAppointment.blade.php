@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Service</h2>
            <div class="row">
              <div class="col-xs-12">
        
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Register Id</th>
                      <th>date</th>
					  <th>Name</th>
                      <th>Token Number</th>
                      <th>status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['appointments'])>0)
                    @foreach ($result['appointments'] as $key=>$appointments)
                        <tr>
                            <td>{{ $appointments->id }}</td>
                            <td>{{ $appointments->regid }}</td>
                            <td>{{ $appointments->datee }}</td>
							<td>{{ $appointments->name }}</td>
                            <td>{{ $appointments->tokenno }}</td>
                            <td>{{ $appointments->status }}</td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editAppointment/{{ $appointments->id }}" class="badge bg-light-blue">edit</a> 
                            
                           
                        </tr>
                    @endforeach
                    @else
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  {{$result['appointments']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  



@endsection