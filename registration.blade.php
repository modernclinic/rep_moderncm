@extends('layouts.front.apphome')
@section('content')
 <div class="fullsize-container">
                <div class="fullsize-slider">
                    <ul>

                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="http://placehold.it/200x200" data-delay="7000" data-saveperformance="off" data-title="Slide" style="color: white;">
                            <!-- MAIN IMAGE -->
                            <img src="{!! asset('resources/assets/front') !!}/images/banner1.png" alt="fullslide6" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">

                        </li>
                    </ul>
                </div>
            </div>
  <div class="container space-sm">
                    <div class="col-md-12">
					 @include('layouts.errors-and-messages')
                        <div class="book-box row">
                            <div class="book-form">
                                <h3 class="hr-after">Make an appointment</h3>
							 {!! Form::open(array('url' =>'register', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data', 'id' => 'sub')) !!}
                                <div class="row">
                                    <p class="col-md-6 col-sm-6">
                                        <input class="form-control" name="name" type="text" placeholder="Name" required="">
                                    </p>
                                    <p class="col-md-6 col-sm-6">
                                        <input class="form-control" type="text" name="address" placeholder="Address" required="">
                                    </p>
                                </div>


								<div class="row">
                                <p class="col-md-6 col-sm-6">
                                    <input class="form-control " type="text" id="dp" name="dob" placeholder="Date of Birth(format 2019-10-09)" required="">
                                </p>
								 <p class="col-md-6 col-sm-6">
                                    <input class="form-control" type="text" name="place" placeholder="Place" required="">
                                </p>
								</div>


									<div class="row" style="margin-left: 20px;">
									<p class="col-md-6 col-sm-6">
										<input id="radio-1" name="gender" type="radio" value="female" required="required" checked="checked">
										<label for="radio-1" class="radio-label">Female</label>
										<input id="radio-2" name="gender" type="radio" value="male" required="required">
										<label for="radio-2" class="radio-label">Male</label>
									</p>
									<p class="col-md-6 col-sm-6">

										<select class="form-control" name="time" id="time" style="margin-left: -17px;width: 103%;">
											<option value="">Appointment time</option>
											<option value="5am">5am</option>
											<option value="6am">6am</option>
											<option value="7am">7am</option>
											<option value="8am">8am</option>
										</select>
									</p>
									</div>


								<div class="row">
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="text" name="phone" placeholder="Phone Number" required="" onKeyPress="return isNumberKey(event)" maxlength="11">
									</p>
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="text" name="password" placeholder="Password" required="">
									</p>
								</div>

								<div class="row">
									<p class="col-md-6 col-sm-6">
										<input class="form-control" type="date" name="date" placeholder="Appoinment Date" required="" onKeyPress="return isNumberKey(event)" maxlength="11">
									</p>

								</div>

								<div>
									<button id="reply_form_submit" type="submit" class="reply_submit_btn trans_300" value="Submit">
										Submit
									</button>
								</div>

							</div>
                        </div>

                    </div>

                </div>
@endsection
